
# THIS FILE IS GENERATED FROM SETUP.PY
short_version = '1.3.2'
version = '1.3.2'
full_version = '1.3.2'
git_revision = 'c45b7027a79c9633e8ffa84532f4bfa092e510a9'
release = True

if not release:
    version = full_version
