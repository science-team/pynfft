Source: pynfft
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>
Section: science
Priority: optional
Build-Depends: cython3,
               debhelper-compat (= 13),
               dh-sequence-python3,
               libnfft3-dev,
               python3-all-dev,
               python3-pytest <!nocheck>,
               python3-numpy,
               python3-setuptools,
               python3-zombie-imp,
Build-Depends-Indep: python3-sphinx
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/science-team/pynfft
Vcs-Git: https://salsa.debian.org/science-team/pynfft.git
Homepage: https://github.com/ghisvail/pyNFFT
Rules-Requires-Root: no

Package: python3-pynfft
Architecture: any
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Provides: ${python3:Provides}
Suggests: python-pynfft-doc
Description: Python bindings for the NFFT3 library - Python 3
 This package provides Python bindings to the NFFT library, useful for
 performing Fourier transforms on non-uniformly sampled data with efficient
 speed. The bindings were generated using Cython and abstract the creation
 and execution of NFFT plans out using classes.
 .
 This package provides the Python 3 version of the bindings.

Package: python-pynfft-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Multi-Arch: foreign
Description: Python bindings for the NFFT3 library - Documentation
 This package provides Python bindings to the NFFT library, useful for
 performing Fourier transforms on non-uniformly sampled data with efficient
 speed. The bindings were generated using Cython and abstract the creation
 and execution of NFFT plans out using classes.
 .
 This package provides the common documentation.
